## 1.2.2

* Upgrade dependencies

## 1.2.1

* Improve code

## 1.2.0

* Refactoring

## 1.0.8

* Improve Firefox compatibility

## 1.0.7

* Fix pick files

## 1.0.6

* Improve layout

## 1.0.5

* Fixes

## 1.0.4

* Improve focus management & UI

## 1.0.3

* Fixes and Improvements

## 1.0.2

* Add FileUploadFormField

## 1.0.1

* Fix reading file data

## 1.0.0

* Initial release.