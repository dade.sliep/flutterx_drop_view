import 'package:cross_file/cross_file.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_drop_view/flutterx_drop_view.dart';
import 'package:flutterx_forms/flutterx_forms.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutterx DropView Demo',
      theme: ThemeData(primarySwatch: Colors.deepOrange).applyAppTheme(),
      home: const DropViewExample());
}

extension _ThemeDataExt on ThemeData {
  ThemeData applyAppTheme() => copyWith(
      outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(backgroundColor: Colors.blueAccent, side: const BorderSide())),
      inputDecorationTheme: InputDecorationTheme(
          labelStyle: const TextStyle(),
          floatingLabelStyle: TextStyle(color: colorScheme.primary),
          border: const OutlineInputBorder(),
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: colorScheme.primary, width: 2))));
}

class DropViewExample extends StatefulWidget {
  const DropViewExample({Key? key}) : super(key: key);

  @override
  State<DropViewExample> createState() => _DropViewExampleState();
}

class _DropViewExampleState extends State<DropViewExample> {
  final MutableLiveData<XFile?> _file = MutableLiveData();
  final GlobalKey<FormState> _form = GlobalKey();

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text('DropView example')),
      body: Form(
        key: _form,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Container(width: kMinInteractiveDimension * 8, padding: const EdgeInsets.all(8), child: TextFormField()),
          Container(
              width: kMinInteractiveDimension * 8,
              padding: const EdgeInsets.all(8),
              child: FileUploadFormField(
                  validator: [
                    nonNull(message: 'No file selected'),
                    fileMimeType(const {'application/pdf'}),
                  ].validator,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  onMimeTypeNotMatch: (error) =>
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(error.toString()))),
                  value: _file)),
          ElevatedButton(onPressed: () => _form.currentState?.validate(), child: const Text('VALIDATE')),
        ]),
      ));
}
