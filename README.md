# flutterx_drop_view

A widget where you can drop files into it, and also pick files from system file browser.

## Import

Import the library like this:

```dart
import 'package:flutterx_drop_view/flutterx_drop_view.dart';
```

## Usage

Check the documentation in the desired source file of this library