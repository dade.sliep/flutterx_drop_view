import 'package:cross_file/cross_file.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_drop_view/flutterx_drop_view.dart';
import 'package:flutterx_forms/flutterx_forms.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class FileUploadFormField extends _FileFormField<XFile> {
  FileUploadFormField({
    super.key,
    super.focusNode,
    super.decoration = const InputDecoration(),
    super.style,
    super.suffixIcon = const Icon(Icons.upload),
    super.canDrop = true,
    super.mimeTypes = const {},
    super.onMimeTypeNotMatch,
    super.value,
    super.onSaved,
    super.validator,
    super.enabled = true,
    super.autovalidateMode,
    super.restorationId,
  }) : super(multiple: false);

  @override
  XFile? _data(List<XFile> files) => files.firstOrNull;

  @override
  String _text(XFile? data) => data?.name ?? '';
}

class MultipleFileUploadFormField extends _FileFormField<List<XFile>> {
  MultipleFileUploadFormField({
    super.key,
    super.focusNode,
    super.decoration = const InputDecoration(),
    super.style,
    super.suffixIcon = const Icon(Icons.upload),
    super.canDrop = true,
    super.mimeTypes = const {},
    super.onMimeTypeNotMatch,
    super.value,
    super.onSaved,
    super.validator,
    super.enabled = true,
    super.autovalidateMode,
    super.restorationId,
  }) : super(multiple: true);

  @override
  List<XFile>? _data(List<XFile> files) => files;

  @override
  String _text(List<XFile>? data) => data?.map((file) => file.name).join(', ') ?? '';
}

abstract class _FileFormField<T> extends FormField<T> {
  final FocusNode? focusNode;
  final InputDecoration decoration;
  final TextStyle? style;
  final Widget suffixIcon;
  final bool canDrop;
  final Set<String> mimeTypes;
  final OnMimeTypeNotMatch? onMimeTypeNotMatch;
  final MutableLiveData<T?>? value;
  final bool multiple;

  _FileFormField({
    super.key,
    required this.focusNode,
    required this.decoration,
    required this.style,
    required this.suffixIcon,
    required this.canDrop,
    required this.mimeTypes,
    required this.onMimeTypeNotMatch,
    required this.value,
    required this.multiple,
    required super.onSaved,
    required super.validator,
    required super.enabled,
    required super.autovalidateMode,
    required super.restorationId,
  }) : super(
            initialValue: value?.valueOrNull,
            builder: (field) => (field as _FileUploadFormFieldState<T>)._buildTextField());

  @override
  FormFieldState<T> createState() => _FileUploadFormFieldState<T>();

  T? _data(List<XFile> files);

  String _text(T? data);

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<FocusNode?>('focusNode', focusNode))
      ..add(DiagnosticsProperty<InputDecoration>('decoration', decoration))
      ..add(DiagnosticsProperty<TextStyle?>('style', style))
      ..add(DiagnosticsProperty<bool>('canDrop', canDrop))
      ..add(IterableProperty<String>('mimeTypes', mimeTypes))
      ..add(ObjectFlagProperty<OnMimeTypeNotMatch?>.has('onMimeTypeNotMatch', onMimeTypeNotMatch))
      ..add(DiagnosticsProperty<MutableLiveData<T?>?>('liveData', value))
      ..add(DiagnosticsProperty<bool>('multiple', multiple));
  }
}

class _FileUploadFormFieldState<T> extends FormFieldState<T> with ObserverMixin, StateObserver {
  final TextEditingController _controller = TextEditingController();
  late final WidgetDataSync<FocusNode> _focusNode =
      WidgetDataSync(fallback: FocusNode.new, provide: () => widget.focusNode);
  late final FocusNode _dragFocusNode = _DragFocus();
  final MutableLiveData<bool> _dragging = MutableLiveData(value: false);
  bool _wasDragging = false;
  bool _registered = false;

  @override
  _FileFormField<T> get widget => super.widget as _FileFormField<T>;

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    super.restoreState(oldBucket, initialRestore);
    _registered = true;
  }

  @override
  void registerObservers() =>
      widget.value?.observe(this, (value) => _registered ? didChange(value) : null, dispatcher: Dispatcher.endOfFrame);

  @override
  void didChange(T? value) {
    super.didChange(value);
    widget.value?.value = value;
  }

  @override
  void didUpdateWidget(covariant FormField<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    _focusNode.sync();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  Widget _buildTextField() => widget.canDrop && widget.enabled
      ? IntrinsicHeight(
          child: RawDropView(
              mimeTypes: widget.mimeTypes,
              throwOnMimeTypeNotMatch: widget.onMimeTypeNotMatch != null,
              multiple: widget.multiple,
              dragging: _dragging,
              onDrop: (stream) => stream.toList().then<void>(_update, onError: _handleError),
              child: LiveDataBuilder<bool>(
                  data: _dragging,
                  builder: (context, dragging) {
                    _updateFocus(dragging: dragging);
                    return _buildTextFieldInternal(dragging: dragging);
                  })))
      : _buildTextFieldInternal();

  Widget _buildTextFieldInternal({bool dragging = false}) {
    final focusNode = _focusNode.data;
    final effectiveDecoration = widget.decoration.applyDefaults(Theme.of(context).inputDecorationTheme);
    final text = widget._text(value);
    return TextField(
        controller: _controller..text = text,
        focusNode: dragging ? _dragFocusNode : focusNode,
        decoration: effectiveDecoration.copyWith(
            errorText: errorText,
            filled: dragging ? true : effectiveDecoration.filled,
            suffixIcon: effectiveDecoration.suffixIcon ??
                InputDecoratorSuffix(
                    builder: (context, params) => SizedBox(
                        height: params.height,
                        width: kMinInteractiveDimension * 1.5,
                        child: OutlinedButton(
                            focusNode: focusNode,
                            style: OutlinedButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                padding: params.padding,
                                side: const BorderSide(width: .5, color: Colors.black45),
                                shape: RoundedRectangleBorder(borderRadius: params.innerBorderRadius)),
                            onPressed: widget.enabled ? _pickFiles : null,
                            child: widget.suffixIcon))),
            suffix: effectiveDecoration.suffix ??
                (text.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsetsDirectional.only(end: 12, top: 8),
                        child: InkResponse(
                            onTap: () => _update(const []),
                            radius: 12,
                            canRequestFocus: false,
                            child: const Icon(Icons.clear, size: 16)))
                    : null),
            fillColor: dragging
                ? effectiveDecoration.filled ?? false
                    ? effectiveDecoration.fillColor?.brighten(-.12) ?? Colors.black12
                    : Colors.black12
                : null),
        style: widget.style,
        textInputAction: TextInputAction.done,
        readOnly: true,
        onSubmitted: (_) => _pickFiles(),
        onEditingComplete: () {},
        enabled: widget.enabled);
  }

  void _updateFocus({required bool dragging}) {
    if (dragging && !_wasDragging)
      _focusNode.data.requestFocus();
    else if (_wasDragging) _focusNode.data.unfocus();
    _wasDragging = dragging;
  }

  void _handleError(dynamic error) =>
      error is MimeTypeNotMatchException ? widget.onMimeTypeNotMatch?.call(error) : throw error;

  void _update(List<XFile> files) => didChange(widget._data(files));

  void _pickFiles() {
    final focusNode = _focusNode.data..requestFocus();
    RawDropView.pickFiles(
            mimeTypes: widget.mimeTypes,
            multiple: widget.multiple,
            throwOnMimeTypeNotMatch: widget.onMimeTypeNotMatch != null)
        .toList()
        .then((files) => files.isNotEmpty ? _update(files) : null, onError: _handleError)
        .whenComplete(focusNode.unfocus);
  }
}

class _DragFocus extends FocusNode {
  @override
  bool get hasFocus => true;
}

ValidationParam<XFile?> fileMimeType(Set<String> mimeTypes, {dynamic message}) {
  assert(mimeTypes.isNotEmpty && !mimeTypes.contains('*/*'), 'useless validation');
  return ValidationParam((file) => file == null || file.mimeType != null && mimeTypes.contains(file.mimeType),
      message:
          message is ValueGetter<String> ? message : () => message ?? 'allowed file types: ${mimeTypes.join(', ')}');
}

ValidationParam<List<XFile>> filesMimeType(Set<String> mimeTypes, {dynamic message}) {
  assert(mimeTypes.isNotEmpty && !mimeTypes.contains('*/*'), 'useless validation');
  return ValidationParam((files) => files.every((file) => file.mimeType != null && mimeTypes.contains(file.mimeType)),
      message:
          message is ValueGetter<String> ? message : () => message ?? 'allowed file types: ${mimeTypes.join(', ')}');
}
