import 'package:cross_file/cross_file.dart';

typedef OnFileDrop = void Function(Stream<XFile> files);
typedef OnMimeTypeNotMatch = void Function(MimeTypeNotMatchException error);

class MimeTypeNotMatchException implements Exception {
  final Set<String> acceptMimeTypes;
  final String? mimeType;

  const MimeTypeNotMatchException(this.acceptMimeTypes, this.mimeType)
      : assert(acceptMimeTypes.length > 0, 'acceptMimeTypes must not be empty');

  @override
  String toString() =>
      'MimeTypeMatchException: accept mimeTypes: {${acceptMimeTypes.join(' ')}}, mimeType: ${mimeType ?? 'undefined'}';
}
