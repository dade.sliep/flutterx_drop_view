import 'package:cross_file/cross_file.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_drop_view/flutterx_drop_view.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class FlutterxDropView extends StatelessWidget {
  final FocusNode? focusNode;
  final DragOperation dragOperation;
  final SystemMouseCursor? cursor;
  final Set<String> mimeTypes;
  final OnMimeTypeNotMatch? onMimeTypeNotMatch;
  final MutableLiveData<XFile?>? file;
  final LiveList<XFile>? files;
  final MutableLiveData<bool>? dragging;
  final Widget? child;

  const FlutterxDropView.single({
    super.key,
    this.focusNode,
    this.dragOperation = DragOperation.copy,
    this.cursor,
    this.mimeTypes = const {},
    this.onMimeTypeNotMatch,
    required this.file,
    this.dragging,
    this.child,
  }) : files = null;

  const FlutterxDropView.multiple({
    super.key,
    this.focusNode,
    this.dragOperation = DragOperation.copy,
    this.cursor,
    this.mimeTypes = const {},
    this.onMimeTypeNotMatch,
    required this.files,
    this.dragging,
    this.child,
  }) : file = null;

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<FocusNode>('focusNode', focusNode))
      ..add(EnumProperty<DragOperation>('dragOperation', dragOperation))
      ..add(DiagnosticsProperty<SystemMouseCursor?>('cursor', cursor))
      ..add(IterableProperty<String>('mimeTypes', mimeTypes))
      ..add(ObjectFlagProperty<OnMimeTypeNotMatch?>.has('onMimeTypeNotMatch', onMimeTypeNotMatch))
      ..add(DiagnosticsProperty<MutableLiveData<XFile?>?>('file', file))
      ..add(IterableProperty<XFile>('files', files))
      ..add(DiagnosticsProperty<MutableLiveData<bool>?>('dragging', dragging));
  }

  @override
  Widget build(BuildContext context) => RawDropView(
      focusNode: focusNode,
      dragOperation: dragOperation,
      cursor: cursor,
      mimeTypes: mimeTypes,
      throwOnMimeTypeNotMatch: onMimeTypeNotMatch != null,
      multiple: files != null,
      dragging: dragging,
      onDrop: _handleDrop,
      child: child);

  void _handleDrop(Stream<XFile> files) => files.toList().then<void>(_handleFiles,
      onError: (error) => error is MimeTypeNotMatchException ? onMimeTypeNotMatch?.call(error) : null);

  void _handleFiles(List<XFile> value) {
    files?.setTo(value);
    file?.value = value.iterator.let((iterator) => iterator.moveNext() ? iterator.current : null);
  }

  static Future<List<XFile>?> pickFiles(
          {Set<String> mimeTypes = const {}, LiveList<XFile>? files, bool throwOnMimeTypeNotMatch = false}) =>
      RawDropView.pickFiles(mimeTypes: mimeTypes, multiple: true, throwOnMimeTypeNotMatch: throwOnMimeTypeNotMatch)
          .toList()
          .then((value) {
        final result = value.isNotEmpty ? value : null;
        if (result != null) files?.setTo(result);
        return result;
      });

  static Future<XFile?> pickFile(
          {Set<String> mimeTypes = const {}, MutableLiveData<XFile?>? file, bool throwOnMimeTypeNotMatch = false}) =>
      RawDropView.pickFiles(mimeTypes: mimeTypes, throwOnMimeTypeNotMatch: throwOnMimeTypeNotMatch)
          .toList()
          .then((value) {
        final result = value.iterator.let((iterator) => iterator.moveNext() ? iterator.current : null);
        if (result != null) file?.value = result;
        return result;
      });
}
