import 'dart:async';
import 'dart:html'; // ignore: avoid_web_libraries_in_flutter
import 'dart:ui_web';

import 'package:cross_file/cross_file.dart';
import 'package:cross_file/src/web_helpers/web_helpers.dart'; // ignore: implementation_imports
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterx_drop_view/src/drag_operation.dart';
import 'package:flutterx_drop_view/src/raw_drop_view_common.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';
import 'package:mime/mime.dart';

class RawDropView extends StatefulWidget {
  final FocusNode? focusNode;
  final DragOperation dragOperation;
  final SystemMouseCursor? cursor;
  final Set<String> mimeTypes;
  final bool throwOnMimeTypeNotMatch;
  final bool multiple;
  final MutableLiveData<bool>? dragging;
  final OnFileDrop onDrop;
  final Widget? child;

  const RawDropView({
    super.key,
    this.focusNode,
    this.dragOperation = DragOperation.copy,
    this.cursor,
    this.mimeTypes = const {},
    this.throwOnMimeTypeNotMatch = false,
    this.multiple = false,
    this.dragging,
    required this.onDrop,
    required this.child,
  });

  @override
  State<RawDropView> createState() => _RawDropViewState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<FocusNode>('focusNode', focusNode))
      ..add(EnumProperty<DragOperation>('dragOperation', dragOperation))
      ..add(DiagnosticsProperty<SystemMouseCursor?>('cursor', cursor))
      ..add(IterableProperty<String>('mimeTypes', mimeTypes))
      ..add(DiagnosticsProperty<bool>('throwOnMimeTypeNotMatch', throwOnMimeTypeNotMatch))
      ..add(DiagnosticsProperty<bool>('multiple', multiple))
      ..add(DiagnosticsProperty<MutableLiveData<bool>?>('dragging', dragging))
      ..add(ObjectFlagProperty<OnFileDrop>.has('onDrop', onDrop));
  }

  static Stream<XFile> pickFiles(
      {Set<String> mimeTypes = const {}, bool multiple = false, bool throwOnMimeTypeNotMatch = false}) async* {
    final picker = FileUploadInputElement();
    final safari = isSafari();
    if (safari) document.body!.append(picker);
    picker.multiple = multiple;
    if (mimeTypes.isNotEmpty && !mimeTypes.contains('*/*')) picker.accept = mimeTypes.join(',');
    picker.click();
    await (picker.onChange.first |
        window.onFocus.first.then((_) => Future.delayed(const Duration(milliseconds: 500), () => _)));
    final files = picker.files ?? const [];
    if (safari) picker.remove();
    yield* _readFromFiles(files, mimeTypes, throwOnMimeTypeNotMatch);
  }

  static Stream<XFile> _readFromFiles(
      Iterable<File> files, Set<String> mimeTypes, bool throwOnMimeTypeNotMatch) async* {
    if (files.isEmpty) return;
    final checkMimeType = mimeTypes.isNotEmpty && !mimeTypes.contains('*/*');
    for (final file in files) {
      final mimeType = lookupMimeType(file.name);
      if (checkMimeType && (mimeType == null || !mimeTypes.contains(mimeType))) {
        if (throwOnMimeTypeNotMatch)
          throw MimeTypeNotMatchException(mimeTypes, mimeType);
        else
          continue;
      }
      yield await RawDropView._readFromFile(file, mimeType);
    }
  }

  static Future<XFile> _readFromFile(File file, String? mimeType) {
    final reader = FileReader()..readAsArrayBuffer(file);
    return reader.onLoadEnd.first.then((_) {
      final bytes = reader.result as Uint8List?;
      if (bytes == null) throw Exception('Cannot read bytes from Blob. Is it still available?');
      DateTime? lastModified;
      try {
        lastModified = file.lastModifiedDate;
      } catch (_) {
        // Firefox workaround
      }
      return XFile.fromData(bytes,
          mimeType: mimeType, name: file.name, length: bytes.length, lastModified: lastModified);
    });
  }
}

class _RawDropViewState extends State<RawDropView> {
  late final String _viewType = 'drop-view-$hashCode';
  late final DivElement _element = DivElement()
    ..style.pointerEvents = 'auto'
    ..style.border = 'none'
    ..style.cursor = widget.cursor?.kind.snakeCase.replaceAll('_', '-')
    ..style.width = '100%'
    ..style.height = '100%'
    ..addEventListener('dragover', _onDragOver)
    ..addEventListener('dragleave', _onDragLeave)
    ..addEventListener('drop', _onDrop);

  @override
  void initState() {
    super.initState();
    platformViewRegistry.registerViewFactory(_viewType, (viewId) => _element..id = 'drop-view-$viewId');
  }

  @override
  void didUpdateWidget(covariant RawDropView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.cursor != widget.cursor) _element.style.cursor = widget.cursor?.kind.snakeCase.replaceAll('_', '-');
  }

  void _onDragOver(Event event) {
    if (event is! MouseEvent) return;
    event.preventDefault();
    event.dataTransfer.dropEffect = widget.dragOperation.name;
    widget.dragging?.value = true;
  }

  void _onDragLeave(Event event) {
    if (event is! MouseEvent) return;
    event.preventDefault();
    widget.dragging?.value = false;
  }

  void _onDrop(Event event) {
    if (event is! MouseEvent) return;
    event.preventDefault();
    final stream = RawDropView._readFromFiles(_eventFiles(event), widget.mimeTypes, widget.throwOnMimeTypeNotMatch);
    widget.onDrop(stream);
    widget.dragging?.value = false;
  }

  @override
  Widget build(BuildContext context) {
    final focusable = widget.focusNode != null;
    final dropView = Focus(
        focusNode: widget.focusNode,
        canRequestFocus: focusable ? null : false,
        skipTraversal: focusable ? null : true,
        descendantsAreFocusable: focusable ? null : false,
        descendantsAreTraversable: focusable ? null : false,
        includeSemantics: false,
        child: HtmlElementView(viewType: _viewType));
    final child = widget.child;
    return child == null
        ? dropView
        : Stack(fit: StackFit.passthrough, alignment: Alignment.center, children: [dropView, child]);
  }

  static Iterable<File> _eventFiles(MouseEvent event) sync* {
    final files = event.dataTransfer.files;
    if (files != null) {
      yield* files;
    } else {
      final items = event.dataTransfer.items;
      if (items != null) {
        for (var i = 0; i < (items.length ?? 0); i++) {
          final item = items[i];
          if (item.kind != 'file') throw StateError('Invalid transfer item kind: ${item.kind}, expected file');
          final file = item.getAsFile();
          if (file == null) throw Exception('Cannot read transfer item as file. Is it still available?');
          yield file;
        }
      }
    }
  }
}
