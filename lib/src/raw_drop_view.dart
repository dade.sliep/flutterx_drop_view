import 'package:cross_file/cross_file.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_drop_view/src/drag_operation.dart';
import 'package:flutterx_drop_view/src/raw_drop_view_common.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

class RawDropView extends StatefulWidget {
  final FocusNode? focusNode;
  final DragOperation dragOperation;
  final SystemMouseCursor? cursor;
  final Set<String> mimeTypes;
  final bool throwOnMimeTypeNotMatch;
  final bool multiple;
  final MutableLiveData<bool>? dragging;
  final OnFileDrop onDrop;
  final Widget? child;

  const RawDropView({
    super.key,
    this.focusNode,
    this.dragOperation = DragOperation.copy,
    this.cursor,
    this.mimeTypes = const {},
    this.throwOnMimeTypeNotMatch = false,
    this.multiple = false,
    this.dragging,
    required this.onDrop,
    this.child,
  });

  @override
  State<RawDropView> createState() => _RawDropViewState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<FocusNode>('focusNode', focusNode))
      ..add(EnumProperty<DragOperation>('dragOperation', dragOperation))
      ..add(DiagnosticsProperty<SystemMouseCursor?>('cursor', cursor))
      ..add(IterableProperty<String>('mimeTypes', mimeTypes))
      ..add(DiagnosticsProperty<bool>('throwOnMimeTypeNotMatch', throwOnMimeTypeNotMatch))
      ..add(DiagnosticsProperty<bool>('multiple', multiple))
      ..add(DiagnosticsProperty<MutableLiveData<bool>?>('dragging', dragging))
      ..add(ObjectFlagProperty<OnFileDrop>.has('onDrop', onDrop));
  }

  static Stream<XFile> pickFiles(
      {Set<String> mimeTypes = const {}, bool multiple = false, bool throwOnMimeTypeNotMatch = false}) async* {}
}

class _RawDropViewState extends State<RawDropView> {
  @override
  Widget build(BuildContext context) => widget.child ?? const SizedBox.shrink();
}
