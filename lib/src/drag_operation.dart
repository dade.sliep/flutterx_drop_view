enum DragOperation {
  copy,
  move,
  link,
  copyMove,
  copyLink,
  linkMove,
  all,
  none,
}
